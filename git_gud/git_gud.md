# Git Gud

(Internet slang) To improve; to develop the necessary skill or
expertise.

- Good Fundamentals
- Good Practices

# I'm not going to sugar coat it

Using `git` is annoying. There are dozens of commands covering hundreds
of edge cases, most of which aren't needed by the individual developer.

- How git came to be
- Everyday Commands For A Developer
- Git Commit
- Git Bisect
- Merging and Rebasing
- Restoring and Reverting
- Using a git forge

# A Brief History of Git

In 2005 Linus Torvalds had a problem.

- Linus himself was the version control tool of choice for the Linux
  Kernel

      Developer writes patch -> Linus' friend reviews patch -> Linus manually adds patch to kernel

- Existing version control software did not meet his needs or that of
  the open source community

Before `git`, the most widely used version control system was the
Concurrent Versions System (CVS), which came with some downsides:

- CVS did not support atomic commits affecting multiple files
- CVS stored file history, not patch history
- CVS was centralized

The last point is important. Linus felt it was critical that each of the
linux kernel's many developers have a discrete copy of the repository
that they could develop their own branches on. This eased offline work
and helped with internal politics. So after much discussion Linus took
two weeks off from kernel development and hacked something together.

# Everyday Commands For Individual Developers

Git users can broadly be grouped into four levels, each level adding a
small set of useful commands for everyday `git`.

Consider `git` is a decentralized version control system, meaning that
it is designed so a developer will have everything they need to manage
the revisions of their individual repositories first and foremost.

<table>
<thead>
<tr>
<th>Command</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>git init</td>
<td>Create an empty git repository</td>
</tr>
<tr>
<td>git branch / git switch</td>
<td>change branches within a repository</td>
</tr>
<tr>
<td>git add</td>
<td>Add file contents to the repository index</td>
</tr>
<tr>
<td>git commit</td>
<td>record changes to the repository</td>
</tr>
<tr>
<td>git restore</td>
<td>Undo changes to the repository</td>
</tr>
<tr>
<td>git merge</td>
<td>merge between local branches</td>
</tr>
<tr>
<td>git tag</td>
<td>mark a commit in the repository</td>
</tr>
</tbody>
</table>

A developer using `git` as their VCS only needs to know the above
commands.

# Everyday Commands For Teams

A developer on a team needs to learn a few additional commands.

<table>
<thead>
<tr>
<th>Command</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>git clone</td>
<td>Create an empty git repository clone a remote repository</td>
</tr>
<tr>
<td>git remote</td>
<td>manage connections to others' repositories</td>
</tr>
<tr>
<td>git push</td>
<td>update remote repository with local changes</td>
</tr>
<tr>
<td>git pull / git fetch</td>
<td>update local repository with remote changes</td>
</tr>
</tbody>
</table>

# Commits

Writing good git commit messages makes things easier for others (and
yourself) to understand the changes due to being able to read about
changes locally. Here is the structure of a gud commit message:

Example:

``` markdown
feat(user): add new authentication flow

This commit introduces a new authentication flow for users,
which includes registration, login, and password reset.

The new flow aims to improve user experience by providing
a more intuitive and secure process. The old authentication
flow was deprecated and all related code has been removed.

Refs #123
```

1.  Separate subject from body with a blank line

2.  Limit the subject line to 50 characters

3.  Do not end the subject line with a period

4.  Use the imperative mood in the subject line

5.  Wrap the body at 72 characters

6.  Use the body to explain what and why vs. how

    *Never* use `git commit -am "<message>"`. Always use a dedicated
    text editor

``` bash
git config --global core.editor "vim" # set a program for git to open for writing commit messages

export GIT_EDITOR=vim # set the editor through an environment variable
```

# Git Bisect

Imagine you have a large project and suddenly, something breaks. You
don't know exactly when the bug was introduced, but you need to find it
quickly.

Git bisect is a powerful tool that helps you find the exact commit that
introduced the bug, saving you a lot of time and effort.

How Git Bisect works

1.  Start the Bisect
    - You tell Git which commit was good and which commit is bad

    - Git then splits the range of commits in half and checks the middle
      commit.

      ``` bash
      git bisect start
      git bisect bad   # The commit where the bug is present
      git bisect good  # A commit where everything worked fine
      ```
2.  Test and Mark Commits
    - Git checks out the middle commit, and you test if the bug is
      present

    - You then mark the commit as good or bad.

      ``` bash
      git bisect good  # If the bug is not present
      git bisect bad   # If the bug is present
      ```
3.  Repeat
    - Git will continue to split the range of commits, checking out the
      middle commit of the remaining range
    - You keep marking the commits as good or bad based on whether the
      bug is present
4.  Identify the Problematic Commit
    - Eventually, git will narrow it down to the eact commit that
      introduced the bug

    - This commit is where the problem started

      ``` bash
      git bisect reset
      ```

Why Use Git Bisect?

- Efficiency: Instead of checking every single commit, Git bisect uses a
  binary search algorithm to quickly find the problematic commit.
- Simplicity: You only need to know two things: which commit is bad and
  which commit is good.
- Precision: Git bisect pinpoints the exact commit that introduced the
  bug, making it easier to understand and fix the issue.

Use Git bisect to keep your project running smoothly and to quickly
address issues when they arise.

# Merging and Rebasing

**Rebasing** and merging are two different ways to integrate changes
from one branch into another in a git repository.

**Merging** is the process of integrating changes from one branch into
another. It creates a new commit (a merge commit) that combines the
histories of the merged branches.

    main:           A---B
                      \
    feature-branch:    C

    main:           A---B---D
                      \   /
    feature-branch:    C

**Rebasing** is the process of moving or combining a sequence of commits
to a new base commit, rewriting the commit history of a branch to make
it appear as if the work was done from a different point in time.

    main:           A---B
                      \
    feature-branch:    C

    main:           A---B
                        \
    feature-branch:      C'

Scenario: You have a `feature-branch` with several commits that you want
to clean up before merging into `main`.

1.  Create and switch to a `feature-branch`
2.  Make several commits on `feature-branch`
3.  View commit history

Now, lets clean up the commits by squashing the typo fix into the
previous commit and possibly reordering or editing the commit messages.
We do this with an **Interactive Rebase**

1.  Start an interactive rebase
2.  Modify the rebase script

**Interactive Rebasing** allows you to:

- Reorder commits
- Squash commits
- Edit commit messages
- Drop commits

By using **Interactive Rebase**, you can create a cleaner, more logical
commit history before integrating your changes into the main branch. It
is *not recommended to rebase the main branch of any repository*.

# Restore and Revert

`git restore` is used to discard changes in the working directory or to
restore files from a specific commit. It is a newer command introduced
to simplify and clarify some actions previously done using
`git checkout` and `git reset`. It is best used to discard uncommitted
changes or reset files to a specific state in your working directory.

`git revert` is used to create a new commit that undoes the changes
introduced by a previous commit. Unlike git reset, which can rewrite
history, git revert maintains the history by adding a new commit. Best
used to undo the effects of a commit while preserving the history.

1.  Revert a specific commit

    ``` bash
    git revert <commit-hash>
    ```

2.  Revert multiple commits

    ``` bash
    git revert <oldest-commit-hash>..<newest-commit-hash>
    ```

3.  Reverting a merge commit

    ``` bash
    git revert -m 1 <merge-commit-hash>
    ```

When to Use:

- When you need to undo changes from a specific commit without removing
  it from the commit history.
- When you want to reverse changes made by a commit in a collaborative
  environment to ensure transparency and maintain a clear history.
- When you need to undo a merge commit while preserving other commits.

# Forge

A Git forge is a web-based platform or service that provides tools and
interfaces for managing Git repositories. It enhances the basic
functionality of Git by offering additional features such as issue
tracking, pull requests, code reviews, project management, continuous
integration (CI), and more. Examples of popular Git forges include
GitHub, GitLab, Bitbucket, and SourceForge (from which the term 'forge'
derives its name).

The key differences between a git forge and git

Git

- Version Control System
- Command-Line Interface
- Portable

Git Forge

- Web-Based Interface
- Enhanced Collaboration
- Project Management
- Fixed
