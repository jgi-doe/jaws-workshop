version 1.0

workflow hello {
  call say_hello { }
  output { 
    File greeting = say_hello.out 
  }
}

task say_hello {
  input {
    String message= "Hello, JGI!"
  }
  command <<< 
    echo ~{message} > final_message.txt
  >>>
  output { 
    File out = "final_message.txt"
  }
  runtime {
    docker: "ubuntu:22.04"
    cpu: 1
    memory: "2G"
  }
}

