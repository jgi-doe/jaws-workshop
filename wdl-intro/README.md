# Intro to Workflow Description Language (WDL)

This is a tutorial for the hands-on activities for our Intro to WDL workshop.

## Install Cromwell

Please follow the instructions provided [here](https://gitlab.com/jgi-doe/jaws-workshop/-/blob/master/wdl-intro/install-java.md?ref_type=heads).

## Hands-on Activity 1: Hello, JGI!

### Locally

- Perform a full validation of the WDL file using WOMtool:

```
java -jar womtool-87.jar validate hello.wdl
# Success!
```

- Run Cromwell locally:

```
java -jar cromwell-87.jar run hello.wdl
```

- Explore the Cromwell folder structure:

```
└── hello
    └── <cromwell_id>
        ├── call-say_hello
        └── execution
            ├── docker_cid
            ├── final_message.txt
            ├── rc
            ├── script
            ├── script.background
            ├── script.submit
            ├── stderr
            ├── stderr.background
            ├── stdout
            ├── stdout.background
            └── tmp
```

### Dori

- Clone the workshop repository on Dori (if you already cloned the repo, don’t forget to git pull):

- Download and run Cromwell on Dori:

```
java -Dconfig.file=$(pwd)/cromwell_dori.conf \
     -Dbackend.providers.Local.config.dockerRoot=$(pwd)/cromwell-executions \
     -Dbackend.default=Local \
     -jar cromwell-87.jar run hello.wdl
```

- Explore the Cromwell folder structure.

## Hands-on Activity 2: Alignment Example

- Create the DAG of the WDL file using WOMtool:

```
cd medium-example/
java -jar womtool-87.jar graph align.wdl > align.dot
dot -Tpng align.dot -o align.png # You need to install graphviz
```

- Install dependencies:

```
brew install graphviz # macOS
sudo apt install graphviz # Linux
```

- Perform a full validation of the WDL file:

```
cd medium-example/
java -jar womtool-87.jar validate align.wdl -i align.json
# Success!
```

- Run bbmap and samtools locally - :fire: Alert! This may fail!!! :fire:

```
java -jar cromwell-87.jar run align.wdl -i align.json
```

- Hint:

    - Add a Docker image to the `runtime {}` section for BOTH tasks and run Cromwell again:

```
runtime {
    docker: "jfroula/jaws-workshop:bbmap"
    cpu: 1
    memory: "1G"
}
```

- Run bbmap and samtools on Dori:

```
java -Dconfig.file=$(pwd)/cromwell_dori.conf \
     -Dbackend.providers.Local.config.dockerRoot=$(pwd)/cromwell-executions \
     -Dbackend.default=Local \
     -jar cromwell-87.jar run align.wdl -i align.json
```

- Explore the Cromwell folder structure.

## Hands-on Activity 3: Run a Public WDL

A BioWDL workflow for gathering QC metrics and preprocessing FASTQ files:

```
git clone --recurse-submodules https://github.com/biowdl/QC.git
cd QC/
java -jar cromwell-87.jar run QC.wdl -i tests/integration/paired_end.json
```

More details can be found [here](https://biowdl.github.io/QC/v2.1.0/index.html).


## Hands-on Activity 4: Build Your Own WDL

Now it's your turn! Go ahead and develop your own WDL. If you have any questions or need assistance, please don't hesitate to contact us.

