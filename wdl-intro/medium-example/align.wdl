version 1.0

workflow bbtools {
    input {
        File reads
        File ref
    }

    call alignment {
       input: fastq=reads,
              fasta=ref
    }

    call samtools {
       input: sam=alignment.sam
    }
   
   output {
       File sorted_bam = samtools.bam
   }
}

task alignment {
    input {
        File fastq
        File fasta
    }

    command {
        bbmap.sh Xmx1g in=~{fastq} ref=~{fasta} out=test.sam
    }

    runtime { 
    }

    output {
       File sam = "test.sam"
    }
}

task samtools {
    input {
        File sam
    }

    command {
       samtools view -b -F0x4 ~{sam} | samtools sort - > test.sorted.bam
    }

    runtime {
    }

    output {
       File bam = "test.sorted.bam"
    }
}
