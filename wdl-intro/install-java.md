# Prework for Intro to WDL Workshop

## Download Cromwell and WOMtool

Download Cromwell and WOMtool from the [Cromwell releases page on GitHub](https://github.com/broadinstitute/cromwell/releases).

```
wget https://github.com/broadinstitute/cromwell/releases/download/87/cromwell-87.jar
wget https://github.com/broadinstitute/cromwell/releases/download/87/womtool-87.jar

```

## Ensure Java Runtime Environment (JRE) Version 11

Check if Java 11 is installed on your machine:

```
java --version
```
Expected output should be similar to:

```
openjdk 11.0.23 2024-04-16
OpenJDK Runtime Environment (build 11.0.23+9-post-Ubuntu-1ubuntu120.04.2)
OpenJDK 64-Bit Server VM (build 11.0.23+9-post-Ubuntu-1ubuntu120.04.2, mixed mode, sharing)
```

If Java 11 is not installed, follow these instructions to install it.

### Install Java 11

**For Mac:**

```
brew install openjdk@11
ln -sfn /opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-11.jdk
echo 'export PATH="/opt/homebrew/opt/openjdk@11/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile
java -version
```

**For Linux (Ubuntu):**

```
sudo apt update
sudo apt install openjdk-11-jre-headless
```

Alternatively, you can download the JDK 11 tarball from the [Oracle JDK 11 archive](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html).

## Clone Workshop Repo

```
git clone https://gitlab.com/jgi-doe/jaws-workshop.git
cd wdl-intro
```

## Try 'Hello' example locally

```
java -jar cromwell-87.jar run hello.wdl
```