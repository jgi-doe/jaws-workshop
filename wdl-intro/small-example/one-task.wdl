version 1.0

workflow example_workflow {
  
  call download_file

  output {
    File final_report = download_file.output_file
  }
}

task download_file {
  input {
    # Hardcoded URL to download the file
    String url = "http://example.com/data.txt"
    String output_name = "downloaded_file.txt"
  }

  command {
    curl -o ~{output_name} ~{url}
  }

  output {
    File output_file = "~{output_name}"
  }

  runtime {
    #docker: "ubuntu:latest" # this does not have curl installed
    #docker: "jfroula/jaws-workshop:curl" # curl is installed
  }
}

