version 1.0

workflow hello {
  input {
    Array[String] message
    String label 
  }
  
  # run multiple instances of "say_hello" in parallel. These
  # jobs are called "shards".
  scatter (ms in message) {
    call say_hello {
      input:
        message = ms,
        label = label 
    }
  }
  
  call cat {
    input:
      outfile = say_hello.outfile 
  }
  
  output { 
    File out_final = "final.txt"
   }
   
  meta {
    author: "JAWS Team"
    email: "jaws-support@lbl.gov"
  }
}


task say_hello {
  input {
    String message
    String label="hello_output" 
  }
  
  command <<< 
    echo ~{message} > ~{label}.txt
  >>>
  
  output { 
     File outfile = "~{label}.txt"
  }

  runtime {
    docker: "ubuntu:22.04"
    cpu: 1
    memory: "2G"
  }
  
  parameter_meta {
    message: "Message to be printed by echo command"
    label: "File name"
  }
}

task cat {
  input {
    Array[File] outfile
  }
  
  command <<< 
    cat ~{sep=' ' outfile} > final.txt
  >>>
  
  output { 
     File out_final = "final.txt"
  }

  runtime {
    docker: "ubuntu:22.04"
    cpu: 1
    memory: "2G"
  }
  
  parameter_meta {
    outfile: "Individual message"
  }
}


