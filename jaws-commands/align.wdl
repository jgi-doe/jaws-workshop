version 1.0

workflow bbtools {
    input {
        File reads
        File ref
    }

    call alignment {
       input: fastq=reads,
              fasta=ref
    }
    call samtools {
       input: sam=alignment.sam
   }
   output {
       File bam = samtools.bam
    }
}

task alignment {
    input {
        File fastq
        File fasta
    }

    command {
        bbmap.sh Xmx12g in=~{fastq} ref=~{fasta} out=test.sam
    }

    runtime {
        docker: "jfroula/aligner-bbmap@sha256:22646e26de6fda84c6997246503b4187903816f3f3e2eb010c3954b2b208f808"
        runtime_minutes: 10
        memory: "5G"
        cpu: 1
    }

    output {
       File sam = "test.sam"
    }
}

task samtools {
    input {
        File sam
    }

    command {
       samtools view -b -F0x4 ~{sam} | samtools sort - > test.sorted.bam
    }

    runtime {
        docker: "jfroula/aligner-bbmap@sha256:22646e26de6fda84c6997246503b4187903816f3f3e2eb010c3954b2b208f808"
        runtime_minutes: 10
        memory: "5G"
        cpu: 1
    }

    output {
       File bam = "test.sorted.bam"
    }
}
