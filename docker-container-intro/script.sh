#!/bin/bash

# If you don't provide any arguments, this script will only check if bbmap.sh and samtools is in your PATH.
# If you do add the two expected arguments, bbmap.sh and samtools will attempt to align the reads to the reference.

READS=$1
REF=$2

function checkFileExist {
	file=$1
	echo -n "checking if $file exists: "
	if [[ ! -e $file ]]; then
		echo no
		exit 1
	fi
		echo yes
}

function checkExecutables {
	echo "checking that bbmap.sh and samtools are available"
    missing=0

	# check bbmap.sh
	which bbmap.sh
	if [[ $? > 0 ]]; then 
		echo "bbmap.sh not found"	
		missing=1
	fi

	# check samtools.sh
	which samtools
	if [[ $? > 0 ]]; then 
		echo "samtools.sh not found"	
		missing=1
	fi

    if [[ $missing ]]; then
		exit 1
	fi
}

############
### MAIN ###
############

if [[ $# < 2 ]]; then
	echo -e "\nUsage: $0 <reads as fastq> <reference as fasta>\n"

	checkExecutables
else
	checkFileExist $1
	checkFileExist $2

	# align reads to reference contigs
	bbmap.sh Xmx12g in=$READS ref=$REF out=test.sam

	# create a bam file from alignment
	samtools view -b -F0x4 test.sam | samtools sort - > test.sorted.bam
fi
