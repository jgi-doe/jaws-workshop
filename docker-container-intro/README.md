# Hands on Demo
The following steps should be done on your local machine so that you have root privelages and can run docker. You need to be root to build docker images.

### Dependancies
See "install_docker.md" if you haven't already.

### On the machine running docker
```
git clone https://gitlab.com/jgi-doe/jaws-workshop.git
cd jaws-workshop/docker-container-intro/
run ./script.sh 
  - it requires two arguments and will run an alignment of fastq reads to a fasta reference.
  - notice that it cannot find the required bbmap.sh and samtools in the $PATH.
```

### The Dockerfile
* The dockerfile sets an environmental variable `ENV DEBIAN_FRONTEND=noninteractive` because the installation of some dependencies using apt-get would otherwise require the user to interact with the terminal.
* By googleing how to download samtools and bbmap (which is part of bbtools), we can find the steps to install each one. Make sure to add the binary to $PATH using `ENV`.
* The working dir will be set to `/bbmap` which means when we run the container (i.e. docker run <image-name> script.sh), the script.sh will be run under `/bbmap` in the container. So it would be logical to mount the sample data to that directory.
* The script.sh gets copied from your cloned repository to the container under a path (i.e. /usr/local/bin/) which is by default in $PATH.

### Build the container
```
docker build --tag bbmap .
# see that it got built
docker images
```

### The docker repository (hub.docker.com)
You need to create an account at docker hub and then create a repository for the image, called something like "bbmap" or whatever you like.  Now you can add images to the repository.
```
docker login
docker tag  bbmap <your-account-name>/bbmap:1.0
docker push <your-account-name>/bbmap:1.0
```
Now check your docker hub repo to see that you have an image saved with the version (or "tag") 1.0.
Note that you need to change the name of the docker container using `tag`. This new name needs to be consistent with what your docker-hub repository namespace is (i.e. jfroula/bbmap) and the "tag" can be anything.

### Run the container
```
docker run bbmap script.sh
```
Note that the two tools that script.sh requires are available in $PATH.

Now run with data
The sample data is in `data` folder but this command fails because there is no `data` folder yet inside the container.
```
docker run bbmap script.sh data/sample.fastq.bz2 data/sample.fasta
```

It's helpful to run your container interactively. You can then see what the container sees and get your command to run successfully & debug.  
```
docker run -it —volume=$(pwd)/data:/bbmap bbmap
```

Mounting the `data` folder into the container is necessary for `script.sh` to access it. We're actually adding the contents of `data` to the `/bbmap` path inside the container.
```
docker run --volume=$(pwd)/data:/bbmap bbmap script.sh sample.fastq.bz2 sample.fasta
# or you can use full paths
docker run --volume=$(pwd)/data:/bbmap bbmap script.sh /bbmap/sample.fastq.bz2 /bbmap/sample.fasta
```

# Creating images that will be portable to other architectures
When you build an image on one architecture like an MacOS M2 processor (arm64) and try to use the image on another architecture, like an intel processor (amd64), it will fail. The solution is to build a container using the [buildx](https://docs.docker.com/reference/cli/docker/buildx/) sub-command.

### buildx
On your local machine, build another container but this time with buildx.
```
docker buildx build --platform linux/amd64,linux/arm64 -t <your-account-name>/bbmap:2.0 --push .
```
The `buildx` sub-command allows you to specify the architectures with `--platform`.  The `--platform` flag is specifying our target architectures, or where we want to run the containers. Two containers are getting built, one for intel(amd64) and one for Mac-M2(arm64). 

Because we've included the --push flag, it automatically will get saved in the docker-hub repository.

### Run the container on dori using apptainer
If you built the first image (1.0) on a Mac, then This command fails since it was not built for the amd64 architecture. If you were using an intel machine, the it should work.
```
apptainer exec --bind $(pwd)/data:/bbmap docker://<your-account-name>/bbmap:1.0 script.sh /bbmap/sample.fastq.bz2 /bbmap/sample.fasta
```

This version (2.0) should work even if you created it on a Mac.
```
apptainer exec --bind $(pwd)/data:/bbmap docker://<your-account-name>/bbmap:2.0 script.sh /bbmap/sample.fastq.bz2 /bbmap/sample.fasta
```

You can go interactive, as with docker, when using apptainer.
```
apptainer shell --bind $(pwd)/data:/bbmap docker://<your-account-name>/bbmap:2.0
```

# Clean up

```
docker buildx prune --all
docker stop $(docker ps -aq)
docker rm -f $(docker ps -aq)
docker rmi -f $(docker images -q)
docker volume rm $(docker volume ls -q)
```

