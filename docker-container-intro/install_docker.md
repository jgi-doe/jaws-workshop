# How to Install Docker

The installation of Docker varies depending on the operating system you're using, but it's a straightforward process across the board. Docker runs seamlessly on macOS, Windows, and Linux. Let's go through the installation steps for each platform.

## How to Install Docker Desktop on macOS
Find the instructions [here](https://docs.docker.com/desktop/install/mac-install/).

## How to Install Docker Desktopon Windows
Find the instructions [here](https://docs.docker.com/desktop/install/windows-install/).

## How to Install Docker on Linux
Find the instructions [here](https://docs.docker.com/desktop/install/linux-install/).

## Verify Installation:

Open the Terminal and execute the following commands:

```
docker --version
docker run hello-world
```
